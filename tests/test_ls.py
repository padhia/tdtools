"tests for ls* commands"
def test_not(capsys):
	from tdtools.ls.db import main
	assert main(["does not exist"]) == 3 # no listing returns code 3
	out, _ = capsys.readouterr()
	assert out == ""

def test_user(capsys):
	from tdtools.ls.user import main
	assert main(["dbtest", "dbc", "syslib", "-hlll"]) == 0
	out, _ = capsys.readouterr()
	assert len(out.rstrip().split('\n')) == 4 # syslib is not a user

def test_db(capsys):
	from tdtools.ls.db import main
	assert main(["dbtest", "-hlll"]) == 0
	out, _ = capsys.readouterr()
	assert len(out.rstrip().split('\n')) == 3

def test_db_only(capsys):
	from tdtools.ls.db import main
	assert main(["dbtest", "-d"]) == 3
	out, _ = capsys.readouterr()
	assert out == ''

def test_tvm(capsys):
	from tdtools.ls.tvm import main
	assert main(["dbtest.test_table", "dbtest.test_view", "-hlll"]) == 0
	out, _ = capsys.readouterr()
	assert len(out.rstrip().split('\n')) == 4

def test_prof(capsys):
	from tdtools.ls.prof import main
	assert main(["dbtest_prof", "-hlll"]) == 0
	out, _ = capsys.readouterr()
	assert len(out.rstrip().split('\n')) == 3

def test_role(capsys):
	from tdtools.ls.role import main
	assert main(["dbtest_role", "-l"]) == 0
	out, _ = capsys.readouterr()
	assert len(out.rstrip().split('\n')) == 3

def test_rmem(capsys):
	from tdtools.ls.rmem import main
	assert main(["dbtest_role.dbtest%", "-l"]) == 0
	out, _ = capsys.readouterr()
	assert len(out.rstrip().split('\n')) == 3
