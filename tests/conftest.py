"global setup including fixtures"
from textwrap import dedent
import pytest

def parse_ddls(ddl_text: str):
	yield from (l.strip() for l in ddl_text.split(';') if l.strip())

def setup_test_schemas(csr):
	ddl_text = dedent("""\
		create role dbtest_role;
		Comment On Role dbtest_role As 'This is a role for dbtest';
		Create Profile dbtest_prof As Account=('$L0$USER&S&D&H'), Default Database=dbtest, Spool=1e9, Temporary=10e6, Password=(Expire=90);
		Create User dbtest From sysdba As Perm=20e6, Password="P@ssw0rd", Profile=dbtest_prof;
		Create database dbtest2 From dbtest As Perm=10e6;
		create database zonedb from sysdba as perm=10e6;
		create zone dbtest_zone root zonedb;

		database dbtest;
		grant dbtest_role to dbtest;

		create multiset table test_table (
			col1 integer not null,
			col2 varchar(128) default user,
			col3 date default date '2000-01-01'
		) primary index(col1);

		insert into test_table(col1) values(1);

		collect stats column(col1) on test_table;

		replace view test_view as locking row for access select * from test_table;

		CREATE TABLE SysDBA.ViewRefs
		( ViewDB    varchar(128) character set unicode not null
		, ViewName  varchar(128) character set unicode not null
		, ViewUpdTS Timestamp(0) not null
		, RefDB     varchar(128) character set unicode
		, RefName   varchar(128) character set unicode
		) PRIMARY INDEX(ViewDB,ViewName);""")

	try:
		for ddl in parse_ddls(ddl_text):
			csr.execute(ddl)
	except:
		pass

def clean_test_schemas(csr):
	ddl_text = dedent("""\
		delete database dbtest2;
		drop database dbtest2;
		delete user dbtest;
		drop user dbtest;
		drop role dbtest_role;
		drop profile dbtest_prof;
		alter zone dbtest_zone drop root;
		drop zone dbtest_zone;
		delete database zonedb;
		drop database zonedb;""")

	for ddl in parse_ddls(ddl_text):
		try:
			csr.execute(ddl)
		except:
			pass

@pytest.fixture(scope="session")
def dbcsr():
	from tdtypes.dbapi import cursor
	with cursor(None) as csr:
		setup_test_schemas(csr)
		csr.schema = 'dbtest'
		yield csr
		clean_test_schemas(csr)
