"tests for vwtree command"

from tdtools.ls.view import main as vwtree

def test_tree(capsys):
	assert vwtree(["-1", "--tree", "dbc.dbcinfov", "dbc.databasesv"]) == 0
	out, _ = capsys.readouterr()
	assert out == """\
DBC.DatabasesV
├─ DBC.ObjectUsage
├─ DBC.Dbase
└─ DBC.Maps
DBC.DBCInfoV
└─ DBC.DBCInfoTbl
"""

def test_tree_not(capsys):
	assert vwtree(["doesnt exist"]) == 3
