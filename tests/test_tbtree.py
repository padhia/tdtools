"tests for tbtree and updrefviews commands"
from textwrap import dedent

def test_viewrefs_create(dbcsr):
	from tdtools.updviewrefs import get_ddl
	dbcsr.execute(get_ddl("ViewRefs"))

def test_updrefviews(capsys, dbcsr):
	from tdtools.updviewrefs import main

	assert main(["--reftb", "ViewRefs", "test_view", "DBC.DBCInfo%"]) is None
	dbcsr.execute("replace view test_view as locking row for access select * from test_table")
	assert main(["--reftb", "ViewRefs", "test_view", "DBC.DBCInfo%"]) is None
	out, _ = capsys.readouterr()
	assert out == dedent("""\
		Views: 3 added, 0 removed, 0 were invalid
		Views: 1 added, 1 removed, 0 were invalid
		""")

def test_tbtree(capsys):
	import tdtools.tbtree as tbt
	assert tbt.main(["--reftb", "ViewRefs", "DBC.DBCInfoTbl"]) is None
	out, _ = capsys.readouterr()
	assert out == dedent("""\
		DBC.DBCInfoTbl
		├─ DBC.DBCInfo
		└─ DBC.DBCInfoV
		""")

def test_viewrefs_drop(dbcsr):
	dbcsr.execute("drop table ViewRefs")
