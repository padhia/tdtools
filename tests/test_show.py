"tests for show* commands"
from textwrap import dedent

def clean(ddl):
	return "\n".join(l.rstrip().replace('\t', '    ') for l in dedent(ddl.rstrip('\n')).split('\n'))

def test_db(capsys):
	from tdtools.show.db import main
	assert main(["dbtest"]) == 0
	out, _ = capsys.readouterr()
	assert clean(out) == """Create User dbtest From sysdba As Perm=10e6, Password="P@ssw0rd", Profile=dbtest_prof, Fallback;"""

def test_view(capsys):
	from tdtools.show.tvm import main
	assert main(["dbtest.test_view"]) == 0
	out, _ = capsys.readouterr()
	assert clean(out) == "replace view test_view as locking row for access select * from test_table;"

def test_grant(capsys):
	from tdtools.show.grant import main
	assert main(["dbtest"]) == 0
	out, _ = capsys.readouterr()
	assert clean(out) == "Grant dbtest_role To dbtest;"

def test_prof(capsys):
	from tdtools.show.prof import main
	assert main(["dbtest_prof"]) == 0
	out, _ = capsys.readouterr()
	assert clean(out) == dedent("""\
		Create Profile dbtest_prof As Account=('$L0$USER&S&D&H'), Default Database=dbtest, Spool=1e9, Temporary=10e6, Password=(Expire=90);""")

def test_role(capsys):
	from tdtools.show.role import main
	assert main(["dbtest_role"]) == 0
	out, _ = capsys.readouterr()
	assert clean(out) == dedent("""\
		Create Role dbtest_role;
		Comment On Role dbtest_role As 'This is a role for dbtest';""")

def test_zone(capsys):
	from tdtools.show.zone import main
	assert main(["dbtest_zone"]) == 0
	out, _ = capsys.readouterr()
	assert clean(out) == "Create Zone dbtest_zone Root zonedb;"

def test_stats(capsys):
	from tdtools.show.stats import main
	assert main(["dbtest.test_table"]) == 0
	out, _ = capsys.readouterr()
	assert clean(out) == clean("""\
		COLLECT STATISTICS
						   -- default SYSTEM SAMPLE PERCENT
						   -- default SYSTEM THRESHOLD PERCENT
					COLUMN ( col1 )
						ON dbtest.test_table ;""")

def test_table(capsys):
	from tdtools.show.tvm import main
	assert main(["dbtest.test_table"]) == 0
	out, _ = capsys.readouterr()
	assert clean(out) == clean("""\
		CREATE MULTISET TABLE dbtest.test_table ,FALLBACK ,
			 NO BEFORE JOURNAL,
			 NO AFTER JOURNAL,
			 CHECKSUM = DEFAULT,
			 DEFAULT MERGEBLOCKRATIO,
			 MAP = TD_MAP1
			 (
			  col1 INTEGER NOT NULL,
			  col2 VARCHAR(128) CHARACTER SET LATIN NOT CASESPECIFIC DEFAULT USER ,
			  col3 DATE FORMAT 'YY/MM/DD' DEFAULT DATE '2000-01-01')
		PRIMARY INDEX ( col1 );

		COLLECT STATISTICS
						   -- default SYSTEM SAMPLE PERCENT
						   -- default SYSTEM THRESHOLD PERCENT
					COLUMN ( col1 )
						ON dbtest.test_table ;""")

def test_table_nostats(capsys):
	from tdtools.show.tvm import main
	assert main(["--no-stats", "dbtest.test_table"]) == 0
	out, _ = capsys.readouterr()
	assert clean(out) == clean("""\
		CREATE MULTISET TABLE dbtest.test_table ,FALLBACK ,
			 NO BEFORE JOURNAL,
			 NO AFTER JOURNAL,
			 CHECKSUM = DEFAULT,
			 DEFAULT MERGEBLOCKRATIO,
			 MAP = TD_MAP1
			 (
			  col1 INTEGER NOT NULL,
			  col2 VARCHAR(128) CHARACTER SET LATIN NOT CASESPECIFIC DEFAULT USER ,
			  col3 DATE FORMAT 'YY/MM/DD' DEFAULT DATE '2000-01-01')
		PRIMARY INDEX ( col1 );""")
