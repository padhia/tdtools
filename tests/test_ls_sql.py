"Test SQL generated from ls module"
from textwrap import dedent
from itertools import zip_longest

def compare_texts(a: str, b: str):
	for s, t in zip_longest(a.splitlines(), b.splitlines()):
		assert s == t

def test_ls_tvm():
	from tdtools.ls import tvm

	sql = tvm.TVM(tvm.__name__, tvm.__doc__, ["--type", "TO", "--size", "+10gb", "prd_rbi%.%", "!%stg%.%", "!%etl%.%", "-1"]).build_sql()
	compare_texts(sql, dedent("""\
		SELECT T.DatabaseName || '.' || T.TableName AS "Table"

		FROM dbc.TablesV T
		LEFT JOIN dbc.StatsV R on R.DatabaseName = T.DatabaseName AND R.TableName = T.TableName AND R.ColumnName IS NULL
		LEFT JOIN (
			SELECT DatabaseName
				, TableName
				, SUM(CurrentPerm) AS TableSize
				, MAX(CurrentPerm) * COUNT(*) AS ImpactSize
				, ImpactSize - TableSize AS WastedSize
				, CAST((1.0 - AVG(CurrentPerm) / NULLIFZERO(MAX(CurrentPerm))) AS DECIMAL(4,3)) AS TableSkew
			FROM dbc.TableSizeV Z
			GROUP BY 1,2
		) Z ON Z.DatabaseName = T.DatabaseName AND Z.TableName = T.TableName

		WHERE CASE
					WHEN T.DatabaseName LIKE '%stg%' THEN NULL
					WHEN T.DatabaseName LIKE '%etl%' THEN NULL
					WHEN T.DatabaseName LIKE 'prd+_rbi%' ESCAPE '+' THEN 0
					ELSE NULL
				END IS NOT NULL
			AND Position(TableKind In 'TO') > 0
			AND TableSize >= 10e9
		ORDER BY T.DatabaseName ASC, T.TableName ASC"""))
