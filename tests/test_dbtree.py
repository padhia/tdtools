"tests for dbtree command"
def trim(out):
	return '\n'.join(s.rstrip() for s in out.split('\n'))

def test_tree(capsys):
	from tdtools.dbtree import main
	assert main(["dbtest"]) == 0
	out, _ = capsys.readouterr()
	assert len(out.rstrip().split('\n')) == 2

def test_tree_long(capsys):
	from tdtools.dbtree import main
	assert main(["dbtest", "-ll"]) == 0
	out, _ = capsys.readouterr()
	assert trim(out) == """\
Database        Alloc       Free   Used  % Skewed  %
---------- ---------- ---------- ------ -- ------ --
dbtest     10,000,000  9,983,616 16,384 0% 16,384 0%
└─ dbtest2 10,000,000 10,000,000      0 0%      0
"""

def test_tree_not(capsys):
	from tdtools.dbtree import main
	assert main(["doesnt exist"]) == 3
	out, _ = capsys.readouterr()
	assert len(out.rstrip().split('\n')) == 1
