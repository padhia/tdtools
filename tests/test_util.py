"tests for util package"
def test_pprint(dbcsr, capsys):
	from tdtools.util import pprint_csr

	pprint_csr(dbcsr, sql="""select 1024 as "Size_", 'N' as ":Ind:"\n""")
	out, _ = capsys.readouterr()
	assert '\n'.join(l.rstrip() for l in out.rstrip().split('\n')) == """\
Size  Ind
----  ---
  1K   N"""
