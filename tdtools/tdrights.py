#! /usr/bin/env python
# -*- coding: utf8 -*-
"Print Teradata permission information on databases and/or to users/roles"

__author__ = "Paresh Adhia"
__copyright__ = "Copyright 2017-2020, Paresh Adhia"
__license__ = "GPL"

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from typing import Dict, Sequence, Union, List, Any, Optional
from dataclasses import dataclass
from tdtypes import DBObjFinder, DBObjPat
from tdtypes.util import Preds, strip_pfx, indent2
from tdtools import util

logger = util.getLogger(__name__)


@dataclass
class AccGroup:
	"Access information grouped by usage"
	level: int
	name: str
	acc: Union[Dict[str, str], List[str]]

	def sqlexpr(self) -> str:
		"return string SQL expression"
		def colexpr(match: str, yes: str, no: str) -> str:
			return f"MAX(CASE AccessRight WHEN '{match}' THEN '{yes}' ELSE '{no}' END)"

		if isinstance(self.acc, Dict):
			xs = (colexpr(m, s, ' ') for m, s in self.acc.items())
		else:
			xs = (colexpr(m, m + ' ', '') for m in self.acc)
		return " ||\n  ".join(xs) + f' AS "{self.name}"'


perms = [
	AccGroup(0, "DML", dict(R='S', I='I', U='U', D='D')),
	AccGroup(0, "Exe", dict(E='M', PE='P', EF='F')),
	AccGroup(0, "+Obj", dict(CT='T', CV='V', CM='M', PC='P', CF='F', CG='G', OP='o', CE='x')),
	AccGroup(2, "-Obj", dict(DT='T', DV='V', DM='M', PD='P', DF='F', DG='G')),
	AccGroup(1, "+Sys", dict(CD='D', CU='U', CR='R', CP='P')),
	AccGroup(2, "-Sys", dict(DD='D', DU='U', DR='R', DP='P')),
	AccGroup(1, "Ops", dict(IX='X', ST='S', DP='D', RS='R')),
	AccGroup(1, "Mon", dict(MS='S', AS='A', MR='R')),
	AccGroup(2, "Misc", [
		'AE', 'AF', 'AP', 'CA', 'CE', 'CP', 'DA', 'GC', 'GD', 'GM', 'NT', 'OD', 'OI',
		'OS', 'OU', 'RF', 'RO', 'SA', 'SD', 'SH', 'SR', 'SS', 'TH', 'UM', 'UT', 'UU'])
]


def buildsql(ac: Sequence[AccGroup], op: Sequence[DBObjPat], users: Optional[Sequence[str]], kind: Optional[List[str]]) -> str:
	"build SQL to fetch given permissions for given objects"
	expr = "\n\n, ".join(a.sqlexpr() for a in ac)
	cols = "\n, ".join(f'"{a.name}"' for a in ac)

	preds = Preds()
	if users is not None:
		preds.append("GRANTEE IN ({})".format(", ".join(f"'{u}'" for u in users)))
	if op:
		preds.append(DBObjFinder(op).sql_pred())
	if kind:
		preds.append("GranteeKind IN ({})".format(','.join(f"'{k}'" for k in kind)))

	return strip_pfx(f"""\
		|WITH Perms AS (
		|    SELECT UserName                    AS Grantee
		|        , COALESCE(d.DBKind, '?')      AS GranteeKind
		|        , a.DatabaseName
		|        , NULLIF(TableName, 'All')     AS TableName
		|        , AccessRight
		|        , CASE WHEN GrantAuthority = 'Y' THEN 'G' ELSE ' ' END AS WithGrant
		|    FROM DBC.AllRightsV a
		|    LEFT JOIN DBC.DatabasesV d ON d.DatabaseName = a.UserName
		|    WHERE UserName <> GrantorName AND UserName <> a.DatabaseName
		| 		AND GranteeKind <> 'R'
		|
		|    UNION All
		|
		|    SELECT RoleName
		|        , 'R'           AS GranteeKind
		|        , DatabaseName
		|        , NULLIF(TableName, 'All')
		|        , AccessRight
		|        , ' '
		|	FROM DBC.AllRoleRightsV
		|	WHERE GranteeKind = 'R'
		|)
		|, PermsPivot AS (
		|    SELECT Grantee
		|        , GranteeKind
		|        , DatabaseName
		|        , TableName
		|        , WithGrant
		|
		|		, {indent2(expr, 2)}
		|
		|   FROM Perms
		|
		|	GROUP BY 1,2,3,4,5
		|)
		|SELECT GranteeKind     AS "U"
		|    , Grantee
		|    , DatabaseName    AS "Database"
		|    , TableName       AS "Table"
		|    , WithGrant       AS "G"
		|
		|	, {indent2(cols)}
		|
		|FROM PermsPivot A
		|{preds.where()}
		|
		|ORDER BY DatabaseName, TableName, GranteeKind, Grantee""")


def main() -> None:
	"script entry-point"
	cli(**vars(getargs()))


def cli(
	obj: Sequence[DBObjPat],
	user: Optional[Sequence[str]],
	verbose: int,
	kind: Optional[List[str]],
	sql: bool,
	**csrargs: Any
) -> None:
	"script main function"
	sqltext = buildsql([p for p in perms if p.level <= verbose], obj, user, kind)
	if sql:
		print(sqltext + ';')
	else:
		with util.cursor(csrargs) as csr:
			csr.execute(sqltext)
			util.pprint_csr(csr)


def getargs() -> Any:
	"get run-time script arguments"
	import argparse

	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument('obj', type=DBObjPat, nargs='*', help=DBObjPat.__doc__)
	parser.add_argument('-u', '--user', nargs='+', help='show permissions granted to only the specified grantees')
	parser.add_argument('-U', '--incl-users', dest='kind', action='append_const', const='U', help='include grantees that are users')
	parser.add_argument('-D', '--incl-db', dest='kind', action='append_const', const='D', help='include grantees that are databases')
	parser.add_argument('-R', '--incl-roles', dest='kind', action='append_const', const='R', help='include grantees that are roles')
	parser.add_argument('-v', '--verbose', default=0, action='count', help='be verbose displaying access information. Use -vv for more details')
	parser.add_argument('-n', '--sql', action='store_true', help="just print sql; do not run it")

	util.dbconn_args(parser)

	return parser.parse_args()


if __name__ == '__main__':
	main()
