#! /usr/bin/env python

from setuptools import setup

with open("README.md", encoding="utf-8") as f:
	readme = f.read()

setup(
	name='tdtools',
	description='A Collection of assorted Teradata Tools',
	long_description=readme,
	long_description_content_type="text/markdown",
	url='https://bitbucket.org/padhia/tdtools',

	author='Paresh Adhia',
	version='0.8.1',
	license='GPL',

	packages=['tdtools', 'tdtools.show', 'tdtools.ls'],
	package_data={'tdtools':['*.json']},
	install_requires=['tdtypes>=0.5.1', 'yappt', 'cached-property'],
	python_requires='>=3.7',

	keywords='teradata',

	entry_points={
		'console_scripts': [
			'lstvm=tdtools.ls.tvm:main',
			'lstb=tdtools.ls.tvm:main',
			'lspr=tdtools.ls.tvm:main',
			'lsvw=tdtools.ls.view:main',
			'lsji=tdtools.ls.tvm:main',
			'lsmc=tdtools.ls.tvm:main',
			'lsfn=tdtools.ls.tvm:main',
			'lspf=tdtools.ls.prof:main',
			'lsrl=tdtools.ls.role:main',
			'lsrm=tdtools.ls.rmem:main',
			'lsus=tdtools.ls.user:main',
			'lsdb=tdtools.ls.db:main',
			'dbtree=tdtools.dbtree:main',
			'tbtree=tdtools.tbtree:main',
			'updviewrefs=tdtools.updviewrefs:main',
			'tptload=tdtools.tptload:main',
			'tptexp=tdtools.tptexp:main',
			'tdexpl=tdtools.tdexpl:main',
			'tdrights=tdtools.tdrights:main',
			'showdb=tdtools.show.db:main',
			'showgrant=tdtools.show.grant:main',
			'showprof=tdtools.show.prof:main',
			'showrole=tdtools.show.role:main',
			'showstats=tdtools.show.stats:main',
			'showtvm=tdtools.show.tvm:main',
			'showzone=tdtools.show.zone:main',
			'helpstats=tdtools.helpstats:main',
		],
	},

	classifiers=[
		'Development Status :: 4 - Beta',
		'License :: OSI Approved :: GNU General Public License (GPL)',

		'Intended Audience :: Developers',
		'Intended Audience :: System Administrators',
		'Intended Audience :: End Users/Desktop',

		'Topic :: Database',
		'Topic :: Utilities',

		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.7',
		'Programming Language :: Python :: 3.8',
	],
)
